function createCard(name, description, pictureUrl, location, starts, ends) {
    const  sDate = new Date(starts)
    let date = sDate.getDate();
    let month = sDate.getMonth();
    month++;
    let year = sDate.getFullYear();
    const start = `${month}/${date}/${year}`
    const  eDate = new Date(ends)
    let date2 = eDate.getDate();
    let month2 = eDate.getMonth();
    month2++;
    let year2 = eDate.getFullYear();
    const end = `${month2}/${date2}/${year2}`
    return`
        <div class="card shadow border-light mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle text-muted mb-2">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-body-secondary">${start} - ${end}</div>
        </div>
 `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log('The response is bad')
      } else {
        const data = await response.json();

        for (let [index, conference] of data.conferences.entries()) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if(detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const location = details.conference.location.name
                const starts = details.conference.starts
                const ends = details.conference.ends
                const html = createCard(name, description, pictureUrl, location, starts, ends);
                const column = document.querySelector(`.column-${index % 3 + 1}`);
                column.innerHTML += html;
         }
       }
    }
  } catch (e) {
      console.error(e);
      console.log('An error was raised')
  }

});
